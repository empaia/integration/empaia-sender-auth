# 0.2.4

* support content-encoding response header

# 0.2.3

* changed fatsapi HTTPException import path

# 0.2.2

* added optional timeout for get, put, post, delete

# 0.2.1

* fix variable type from `AnyHttpUrl` to `str` for `idp_url` in `AuthSettings`

# 0.2.0

* replaced reactive token update mechanism with a preemptive token update routine

# 0.1.3

* added flag for GET, PUT, DELETE, POST to return status_code (default=False)

# 0.1.2

* using urllib for proxy handling

# 0.1.1

* added `ProxySettings`
